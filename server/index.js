import express from 'express'
import  Mongoose  from 'mongoose'
import dotenv from 'dotenv'
dotenv.config();

import Students from './Routes/studentsRoute.js';

const app = express(); 
const PORT = process.env.PORT || 4040; 

app.use(express.json()); 

Mongoose.connect(process.env.MONGODB_URI)  
    .then(() => {
        console.log('Connected to MongoDB'); 
    })                                          
    .catch((error) => {
        console.error('MongoDB connection error:', error);  
    });


    app.use('/students', Students); 


    app.listen(PORT, () => {          
        console.log(`Server listening on port ${PORT}`); 
    });